'use strict';

var express = require('express');
var router = express.Router();
var post = require('../controllers/posts.server.controller');
//Used for routes that must be authenticated.
function isAuthenticated (req, res, next) {
    // if user is authenticated in the session, call the next() to call the next request handler
    // Passport adds this method to request object. A middleware is allowed to add properties to
    // request and response objects

    //allow all get request methods
    if(req.method === "GET"){
        return next();
    }
    if (req.isAuthenticated()){
        return next();
    }

    // if the user is not authenticated then redirect him to the login page
    return res.redirect('/#login');
}

//Register the authentication middleware
router.use('/', isAuthenticated);
router.use('/:id', isAuthenticated);

router.route('/')
    //creates a new post
    .post(post.create)
    //gets all posts
    .get(post.list);

router.route('/:id')
    .get(post.read)
    .put(post.update)
    .delete(post.delete);

module.exports = router;