'use strict';

var express = require('express');
var router = express.Router();
var mongoose = require( 'mongoose' );
var User = mongoose.model('User');

//Used for routes that must be authenticated.
function isAuthenticated (req, res, next) {
    // if user is authenticated in the session, call the next() to call the next request handler
    // Passport adds this method to request object. A middleware is allowed to add properties to
    // request and response objects

    //allow all get request methods
    if(req.method === "GET"){
        return next();
    }
    if (req.isAuthenticated()){
        return next();
    }

    // if the user is not authenticated then redirect him to the login page
    return res.redirect('/#login');
}

//Register the authentication middleware
router.use('/', isAuthenticated);

router.route('/')
    //creates a new post
    .post(function(req, res){

    })
    //gets all posts
    .get(function(req, res){
        User.find(function(err, users){
            console.log('get_users');
            if(err){
                return res.status(500).send(err);
            }
            return res.status(200).send(users);
        });
    });
router.route('/:id')
    //gets specified post
    .get(function(req, res){
        User.findById(req.params.id, function(err, user){
            if(err){
                return res.status(500).send(err);
            }
            res.status(200).json(user)
        });
    })
    //updates specified post
    .put(function(req, res){
        User.findById(req.params.id, function(err, user){
            if(err){
                return res.status(500).send(err);
            }
            user.save(function(err, user){
                if(err){
                    res.status(500).send(err);
                }
                res.status(200).json(user)
            });
        });
    })
    //deletes the post
    .delete(function(req, res, next) {
        User.remove({
            _id: req.params.id
        }, function(err) {
            if (err){
                return res.status(500).send(err);
            }
            res.status(200).json({ status: 'success' })
        });
    });

module.exports = router;