'use strict';

var express = require('express');
var router = express.Router();
var multer  = require('multer');
var storage = multer.memoryStorage();
var upload = multer({ storage: storage });

var fs = require('fs');

router.post('/', upload.single('file'), function(req, res) {
    var fileName = 'public/uploads/' + req.file.originalname;
    fs.stat(fileName, function(err, stats) {
        if (err) {
            console.log(err);
            return; // exit here since stats will be undefined
        }
        if (stats.isFile()) {
            fs.unlink(fileName, function (err) {
                if (err) throw err;
                console.log('successfully deleted ' + fileName);
            });
        }
    });
    fs.writeFile(fileName, req.file.buffer, 'ascii', function (err) {
        if (err) throw err;
        console.log('It\'s saved!');
    });
    res.status(200).json({ file: req.file});
});

module.exports = router;
