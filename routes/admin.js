'use strict';

var express = require('express');
var router = express.Router();


/**
 * isAuthenticated
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
function isAuthenticated (req, res, next) {
    // if user is authenticated in the session, call the next() to call the next request handler
    // Passport adds this method to request object. A middleware is allowed to add properties to
    // request and response objects


    if (req.isAuthenticated()) {
        return next();
    }
    var user = req.user;
    if (typeof user === 'object') {
        if (user.role.type === 'admin') {
            return next();
        }
    }

    // if the user is not authenticated and not admin then redirect him to the login page
    var err = new Error('not allowed!');
    err.status = 403;
    res.render('403', {
        message: '403 Forbidden',
        error: err,
        layout: 'adminlayout'
    });

}

//Register the authentication middleware
router.use('/', isAuthenticated);

//Get Home page
router.route('/')
    .get(function(req, res, next){
        res.render('index', {layout: 'adminlayout' });
    });

module.exports = router;

