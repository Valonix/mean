var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var validator = require('validator');

var postSchema = new mongoose.Schema({
    created_by: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    comments: [{
        text: String,
        postedBy: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User'
        }
    }],
    title: String,
    created_at: {type: Date, default: Date.now},
    text: String,
    image: String
});

var userSchema = new mongoose.Schema({
    username: String,
    email: {
        type: String,
        validate: {
            validator: function(v) {
                return validator.isEmail(v);
            },
            message: '{VALUE} is not a valid email!'
        }
    },
    role: {
        type: {type: String, default: 'user'}
    },
    password: String, //hash created from password
    created_at: {type: Date, default: Date.now}

});

mongoose.model('Post', postSchema);
mongoose.model('User', userSchema);