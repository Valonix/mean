(function() {
    'use strict';

    angular.module('AuthController', [])

        .controller('AuthCtrl', ['$scope', '$cookies', '$http', '$rootScope', '$location', '$window', 'authService',
            function($scope, $cookies, $http, $rootScope, $location, $window, authService){

                /**
                 * Defaults variables
                 */
                
                $rootScope.user = {username: '', password: '', id: null};
                $rootScope.error_message = '';
                $scope.pageClass = 'page-login';

                /**
                 * Use Auth Service to Find User in MongoDB and set this user to rootScope and create cookies.
                 */
                $scope.login = function(){
                    authService.login($rootScope.user)
                        .then(function(response){
                            if(response.state === 'success'){
                                if($rootScope.user.reload){
                                    $window.location.reload();
                                }
                                $location.path('/');
                            }
                            else{
                                $rootScope.gendalf = true;
                                $rootScope.error_message = response.message;
                            }
                        },
                        function(response){
                            $rootScope.error_message = "Some error with register " + response.status;
                        }
                    );
                };
                /**
                 * Use Auth Service to Registering all Users on Server Side and MongoDB.
                 */
                $scope.register = function(){
                    authService.register($rootScope.user)
                        .then(function(response){
                            if(response.state === 'success'){
                                $location.path('/');
                            }
                            else{
                                $rootScope.error_message = response.message;
                            }
                        },
                        function(response){
                            $rootScope.error_message = "Some error with register" + response.status;
                        }
                    );
                };

                /**
                 * Logout
                 */
                $scope.signout = function(){
                    authService.signout();
                };
            }
        ]);

})();

