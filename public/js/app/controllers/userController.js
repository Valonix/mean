(function() {
    'use strict';

    angular.module('UserControllers', [])
        .controller('UserListCtrl', ['$scope', '$routeParams', 'User',
            function($scope, $routeParams, User) {
                var vm = this;
                vm.users = User.query(function(){
                    //console.log(vm.users);
                });

                vm.deleteUser = function(user) {
                    user.$delete(function() {
                        console.log('deleted');
                    });
                };
            }
        ])
        .controller('UserViewController', ['$scope', '$routeParams', 'User', 'user', function($scope, $routeParams, User, user) {
            var vm = this;

            vm.roles = [
                { name: 'Admin', type: 'admin'},
                { name: 'User', type: 'user'}
            ];
            vm.user = user;
        }]);
})();
