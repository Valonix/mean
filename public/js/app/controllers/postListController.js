(function() {
    'use strict';

    angular.module('PostListController', [])
        .controller('PostListCtrl', ['$scope', '$route', '$routeParams', '$location', 'Post',
            function($scope, $route, $routeParams, $location, Post) {
                $scope.$route = $route;
                $scope.$location = $location;
                $scope.$routeParams = $routeParams;

            }
        ]);
})();
