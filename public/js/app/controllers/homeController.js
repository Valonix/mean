(function() {
    'use strict';

    angular.module('HomePageController', [])
            .controller('HomePageController', [ '$cookies', '$scope', '$rootScope', 'Post',
                function( $cookies, $scope, $rootScope, Post) {
                    $scope.posts = Post.query();
                    $scope.newPost = {created_by: null, text: '', created_at: null};
                    $scope.pageClass = 'page-home';

                    $scope.post = function() {
                        $scope.newPost = new Post({
                            created_by : $rootScope.currentUser._id,
                            text: $scope.newPost.text,
                            created_at: Date.now()
                        });
                        $scope.newPost.$save().then(function (response) {
                                if (angular.isObject(response)) {
                                    $scope.posts.push($scope.newPost) ;
                                }
                            },
                            function (err) {
                                console.log(err);
                            }
                        )
                    };

                }
        ]);
})();
