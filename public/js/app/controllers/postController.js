(function() {
    'use strict';

    angular.module('PostControllers', [])
        .controller('PostListCtrl', ['$scope', '$routeParams', 'Post','$log',
            function($scope, $routeParams, Post, $log) {
                var vm = this;
                vm.posts = Post.query(function(){

                });
                vm.letterLimit = 20;
                vm.deletePost = function (post) {
                    var id = post._id;
                    Post.delete({ id : id }, function(){
                        var index = vm.posts.indexOf(post);
                        vm.posts.splice(index, 1);
                    });
                };
            }
        ])
        .controller('PostEditCtrl', ['$scope', '$rootScope', '$routeParams', 'Post', 'Upload', '$log', 'post', function($scope, $rootScope, $routeParams, Post, Upload, $log, post) {
            var vm = this;
            vm.post = post;

            vm.editPost = function(){
                console.log($rootScope.currentUser._id);
                var updatePost = new Post({
                    id: vm.post._id,
                    title: vm.post.title,
                    created_by: $rootScope.currentUser._id,
                    text: vm.post.text,
                    created_at: vm.post.created_at
                });

                updatePost.$update().then(function(response){
                        $log.log(response);
                    },
                    function(err){
                        console.log(err);
                    }
                )
            };
        }])
        .controller('PostAddCtrl', ['$scope', '$rootScope', '$routeParams', 'Post', 'Upload', '$log', '$timeout', function($scope, $rootScope, $routeParams, Post, Upload, $log, $timeout) {
            var vm = this;

            vm.addPost = function () {
                var image = vm.newPost.picFile;
                vm.newPost = new Post({
                    title: vm.newPost.title,
                    created_by: $rootScope.currentUser._id,
                    text: vm.newPost.text,
                    image: null,
                    created_at: Date.now()
                });

                if (typeof image === 'object' || image != null) {
                    vm.newPost.image = image.name;
                }
                vm.newPost.$save().then(function (response) {
                        if (typeof image === 'object') {
                            vm.uploadPic(image);
                        }
                    }, function (err) {
                        console.log(err);
                    }
                )
            };
            vm.uploadPic = function (file) {
                console.log(file);
                file.upload = Upload.upload({
                    url: '/upload/',
                    data: {file: file}
                });

                file.upload.then(function (response) {
                    $timeout(function () {
                        console.log(response.data);
                        file.result = response.data;
                    });
                }, function (response) {
                    if (response.status > 0)
                        vm.errorMsg = response.status + ': ' + response.data;
                }, function (evt) {
                    // Math.min is to fix IE which reports 200% sometimes
                    file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
                });
            }

        }]);
})();
