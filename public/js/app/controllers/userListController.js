(function() {
    'use strict';

    angular.module('UserListController', [])
        .controller('UserListCtrl', ['$scope', '$routeParams', 'User',
            function($scope, $routeParams, User) {
                $scope.name = "UserListCtrl";
                $scope.params = $routeParams;
                var vm = this;
                vm.users = User.query();
            }
        ]);
})();
