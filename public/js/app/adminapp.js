(function() {
    'use strict';
    var adminApp = angular.module('adminApp',
        [
            'ngRoute',
            'ngResource',
            'ngAnimate',
            'ngCookies',
            'ngMessages',
            'ngSanitize',
            'ui.bootstrap',
            'MainController',
            'AuthController',
            'authService',
            'AdminController',
            'PostControllers',
            'postService',
            'UserControllers',
            'userService',
            'mainDirectives',
            'ngFileUpload',
            'textAngular'
        ])
        .run(['$rootScope', '$cookies', '$route', '$location', 'authService', function($rootScope, $cookies, $route, $location, authService) {

            /**
             * Defaults Variables
             * @type {boolean}
             */
            $rootScope.authenticated = false;
            $rootScope.currentUser = {};
            var userCookie = $cookies.getObject('user');
            $rootScope.isCollapsed = true;
            //Auth on server side.
            if(userCookie){
                authService.login(userCookie);
            }


            /**
             * On Each Changing route or first load page Set some data
             */
            $rootScope.$on('$routeChangeSuccess', function() {
                $rootScope.currentUser = authService.getUserInfo();
                if(typeof $route.current.data === 'object'){
                    document.title = $route.current.data.title;
                }
            });

            /**
             * Signout user
             */
            $rootScope.signout = function(){
                authService.signout();
            };


        }]);

        adminApp.config(function($routeProvider){
            $routeProvider
                .when('/', {
                    templateUrl: 'partials/admin/admin.html',
                    controller: 'AdminCtrl',
                    controllerAs: 'Admin',
                    data:{
                        title: 'Admin Page | Donika Admin',
                        auth : true
                    }
                })
                .when('/login', {
                    templateUrl: 'partials/login.html',
                    controller: 'AuthCtrl',
                    controllerAs: 'Auth',
                    data:{
                        title: 'Login Page | Donika Admin',
                        auth: false
                    }
                })
                .when('/posts', {
                    templateUrl: 'partials/admin/posts/posts.html',
                    controller: 'PostListCtrl',
                    controllerAs:'PostList',
                    data:{
                        title: 'Posts Page | Donika Admin',
                        auth: true
                    }
                })
                    .when('/posts/add', {
                        templateUrl: 'partials/admin/posts/post-add.html',
                        controller: 'PostAddCtrl',
                        controllerAs: 'PostAdd',
                        data:{
                            title: 'Add Post | Donika Admin',
                            auth: true
                        }
                    })
                    .when('/posts/:postId', {
                        templateUrl: 'partials/admin/posts/post-view.html',
                        controller: 'PostEditCtrl',
                        controllerAs: 'PostEdit',
                        resolve: {
                            //this gets passed as 'post' to edit controller
                            //we can now use full promise interface and as the model
                            post: ['$route', 'Post', function($route, Post){
                                return Post.get({ id: $route.current.params.postId});
                            }]
                        }
                    })
                .when('/users', {
                    templateUrl: 'partials/admin/users/users.html',
                    controller: 'UserListCtrl',
                    controllerAs:'UserList',
                    data:{
                        title: 'Users Page | Donika Admin',
                        auth: true
                    }
                })
                    .when('/user/:userId', {
                        templateUrl: 'partials/admin/users/user-view.html',
                        controller: 'UserViewController',
                        controllerAs: 'UserView',
                        data:{
                            title: 'Users Page | Donika Admin',
                            auth: true
                        },
                        resolve: {
                            //this gets passed as 'user' to edit controller
                            //we can now use full promise interface and as the model
                            user: ['$route', 'User', function($route, User){
                                return User.get({ id: $route.current.params.userId});
                            }]
                        }
                    })

                .otherwise({redirectTo: '/'});
    });

})();