(function() {
    'use strict';

    angular.module('mainDirectives', [])
        .directive('projectNavItem', function($rootScope, $location) {
            var link = function(scope, element, attrs) {
                var href = element.attr('href');
                $rootScope.$on('$locationChangeSuccess', function() {
                    element.removeClass('active');
                    if (href === $location.path()) {
                        element.addClass('active');
                    }
                });
            };
            return {
                restrict: 'A',
                link: link
            };
        });
})();
