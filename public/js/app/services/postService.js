(function() {
    'use strict';

    angular.module('postService', [])
        .factory('Post', [ '$resource',
            function($resource) {
                return $resource('/posts/:id', {id:'@id'}, {
                    'get':    { method:'GET'},
                    'update': { method:'PUT',
                    'query':  { method:'GET', isArray:true}
                    }
                });
            }
        ]);
})();