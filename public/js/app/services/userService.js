(function() {
    'use strict';

    angular.module('userService', [])
        .factory('User', [ '$resource',
            function($resource) {
                return $resource('/users/:id', {id:'@id'}, {
                    'get':    { method:'GET'},
                    'update': { method:'PUT',
                    'query':  { method:'GET', isArray:true}
                    }
                });
            }
        ]);
})();