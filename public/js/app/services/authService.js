(function() {
    'use strict';

    angular.module( 'authService', [])
            .factory('authService', ['$rootScope', '$http', '$cookies', '$q','$location',
                function($rootScope, $http, $cookies, $q, $location) {

                    var authService = {} ;
                    var expireDate = new Date();
                    expireDate.setDate(expireDate.getDate() + 3);

                    var userCookie = $cookies.getObject('user');
                    var userInfo = {};

                    authService.register = function(data){
                        return $http.post('/auth/signup', data)
                            .then(function successCallback(response) {
                                var res = response.data;
                                if (typeof res === 'object') {
                                    $rootScope.authenticated = true;
                                    $rootScope.currentUser = res.user;
                                    $cookies.putObject('user', res.user, {expires: expireDate});
                                    return res;
                                } else {
                                    // invalid response
                                    return $q.reject(response.data);
                                }
                            }, function errorCallback(response) {
                                return $q.reject(response);
                            });

                    };

                    authService.getUserInfo = function(){
                        if(typeof userInfo === 'object'){
                            return userInfo;
                        }else if (userCookie) {
                            userInfo = userCookie;
                        }
                    };

                    authService.init = function() {
                        if (userCookie) {
                            userInfo = userCookie;
                        }
                    };

                    authService.init();

                    authService.login = function(data){
                        return $http.post('/auth/login', data)
                            .then(function successCallback(response) {
                                var res = response.data;
                                if (typeof res === 'object') {
                                    $cookies.putObject('user', res.user, {expires: expireDate});
                                    $rootScope.authenticated = true;
                                    $rootScope.currentUser = res.user;
                                    userInfo = res.user;
                                    if(res.user.role.type === 'admin'){
                                        $rootScope.admin = true;
                                    }
                                    return res;
                                } else {
                                    // invalid response
                                    return $q.reject(response.data);
                                }
                            }, function errorCallback(response) {
                                return $q.reject(response);
                            });
                    };
                    authService.signout = function(){
                        return $http.get('auth/signout')
                            .then(function successCallback(){
                                $cookies.remove('user');
                                $rootScope.authenticated = false;
                                $rootScope.currentUser = '';
                                $rootScope.admin = false;
                                $location.path('/');

                            }, function errorCallback(response) {
                                return $q.reject(response);
                            });
                    };

                    return authService;
            }
        ]);
})();
