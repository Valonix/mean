(function() {
    'use strict';
    var mainApp = angular.module('mainApp',
        [
            'ngRoute',
            'ngResource',
            'ngAnimate',
            'ngAria',
            'ngCookies',
            'ngMessages',
            'ngSanitize',
            'ui.bootstrap',
            'MainController',
            'AuthController',
            'HomePageController',
            'postService',
            'authService'
        ])
        .run(['$rootScope', '$location', '$cookies', '$route', 'authService', function($rootScope, $location, $cookies, $route, authService) {

            /**
             * Defaults Variables
             * @type {boolean}
             */

            $rootScope.authenticated = false;
            $rootScope.currentUser = {};
            $rootScope.isCollapsed = true;
            var userCookie = $cookies.getObject('user');
            if(userCookie){
                console.log('auth');
                authService.login(userCookie);
            }

            /**
             * On Each Changing route or first load page Set some data
             */
            $rootScope.$on('$routeChangeSuccess', function() {
                $rootScope.currentUser = authService.getUserInfo();
                if(typeof $route.current.data === 'object'){
                    document.title = $route.current.data.title;
                }
            });
            $rootScope.$on('$locationChangeStart', function() {
                $rootScope.previousPage = $location.path();
            });

            $rootScope.signout = function(){
                authService.signout();
            };
    }]);

    mainApp.config(function($routeProvider){
        $routeProvider
            .when('/', {
                templateUrl: 'partials/main.html',
                controller: 'HomePageController',
                data: {
                    title: 'Home Page | Donika Website'
                }
            })
            .when('/login', {
                templateUrl: 'partials/login.html',
                controller: 'AuthCtrl',
                data: {
                    title:'Login Page | Donika Website'
                }
            })
            .when('/register', {
                templateUrl: 'partials/register.html',
                controller: 'AuthCtrl',
                data:{
                    title: 'Sign Up | Donika Website'
                }
            })
            .otherwise({redirectTo: '/'});
    });

})();
