'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
    multer  = require('multer'),
    storage = multer.memoryStorage(),
    upload = multer({ storage: storage }),
    errorHandler = require(path.resolve('./controllers/errors.server.controller')),
    fs = require('fs');


/**
 * Upload a file
 */
exports.upload = function (req, res) {
    fs.writeFile('public/uploads/' + req.file.originalname, req.file.buffer, 'ascii', function (err) {
        if (err) throw err;
        console.log('It\'s saved!');
    });

    res.status(200).json({ file: req.file});
};