'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
    mongoose = require('mongoose'),
    Post = mongoose.model('Post'),
    errorHandler = require(path.resolve('./controllers/errors.server.controller')),
    filesController = require(path.resolve('./controllers/files.server.controller'));

/**
 * Create a post
 */
exports.create = function (req, res) {
    var post = new Post(req.body);
    post.user = req.user;
    post.save(function (err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.json(post);
        }
    });
};

/**
 * Show the current post
 */
exports.read = function (req, res) {
    Post.findById(req.params.id, function(err, post){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        }
        res.status(200).json(post)
    });
};

/**
 * Update a post
 */
exports.update = function (req, res) {
    Post.findById(req.params.id, function(err, post){
        if(err){
            return res.status(500).send(err);
        }
        post.text = req.body.text;
        post.title = req.body.title;

        post.save(function(err, post){
            if(err){
                return res.status(400).send({
                    message: errorHandler.getErrorMessage(err)
                });
            }
            res.status(200).json(post)
        });
    });
};

/**
 * Delete an post
 */
exports.delete = function (req, res) {
    var id = req.params.id;
    deleteImageByName(id);
    Post.findByIdAndRemove(id, {}, function(err, post){
        if (err) throw err;
        res.status(200).json({ status: 'success'});
    });
};

/**
 * List of Posts
 */
exports.list = function (req, res) {
    Post.find({})
        .populate('created_by', 'username')
        .exec(function(err, posts) {
            if (err) {
                return res.status(400).send({
                    message: errorHandler.getErrorMessage(err)
                });
            }
            return res.status(200).send(posts);
        })
};

/**
 * Post middleware
 */
exports.postByID = function (req, res, next, id) {

    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).send({
            message: 'Post is invalid'
        });
    }

    Post.findById(id).populate('created_by', 'username').exec(function (err, post) {
        if (err) {
            return next(err);
        } else if (!post) {
            return res.status(404).send({
                message: 'No post with that identifier has been found'
            });
        }
        req.post = post;
        next();
    });
};

/**
 * Post middleware
 */
var deleteImageByName = function (id) {

    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).send({
            message: 'Post is invalid'
        });
    }
    Post.findById(id, 'image', function(err, post){
        if (err) throw err;
        if(filesController.deletePostImage(post.image)){
            res.status(200).json({ status: 'success'})
        }
    });
};